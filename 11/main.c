#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 150
#define COLS 150

struct point {
    long long int y;
    long long int x;
};

struct point *galaxies = NULL;
int galaxies_count = 0;

struct point **sorted_y;
struct point **sorted_x;

int compare_y(const void *a, const void *b) {
    struct point *point_a = *(struct point**)a;
    struct point *point_b = *(struct point**)b;

    if ( point_a->y < point_b->y)
        return -1;
    else if (point_a->y > point_b->y)
        return 1;
    else
        return 0;
}

int compare_x(const void *a, const void *b) {
    struct point *point_a = *(struct point**)a;
    struct point *point_b = *(struct point**)b;

    if ( point_a->x < point_b->x)
        return -1;
    else if (point_a->x > point_b->x)
        return 1;
    else
        return 0;
}

void sort_y_x() {
    sorted_y = malloc(sizeof(struct point *) * galaxies_count);
    sorted_x = malloc(sizeof(struct point *) * galaxies_count);

    //populate the arrays
    for(int i = 0; i < galaxies_count; ++i) {
        sorted_y[i] = &galaxies[i];
        sorted_x[i] = &galaxies[i];
    }

    //sort them
    qsort(sorted_y, galaxies_count, sizeof(struct point *), compare_y);
    qsort(sorted_x, galaxies_count, sizeof(struct point *), compare_x);
}

int silver() {
    long long int additional_y = 0;
    long long int additional_x = 0;
    int unique_y = 0;
    int unique_x = 0;
    int prev_y = -1;
    int prev_x = -1;
    for(int i = 0; i < galaxies_count; ++i) {
        //if the same y value
        if(prev_y == sorted_y[i]->y) {
            sorted_y[i]->y += additional_y;
        } else {
            ++unique_y;
            prev_y = sorted_y[i]->y;
            additional_y = (sorted_y[i]->y + 1) - unique_y;
            sorted_y[i]->y += additional_y;
        }

        if(prev_x == sorted_x[i]->x) {
            sorted_x[i]->x += additional_x;
        } else {
            ++unique_x;
            prev_x = sorted_x[i]->x;
            additional_x = (sorted_x[i]->x + 1) - unique_x;
            sorted_x[i]->x += additional_x;
        }
    }

    int sum = 0;
    for(int i = 0; i < galaxies_count; ++i)
        for(int j = i + 1; j < galaxies_count; ++j)
            sum += abs(galaxies[i].x - galaxies[j].x) + abs(galaxies[i].y - galaxies[j].y);

    return sum;
}

long long int gold() {
    long long int additional_y = 0;
    long long int additional_x = 0;
    long long int unique_y = 0;
    long long int unique_x = 0;
    int prev_y = -1;
    int prev_x = -1;
    for(int i = 0; i < galaxies_count; ++i) {
        //if the same y value
        if(prev_y == sorted_y[i]->y) {
            sorted_y[i]->y += additional_y;
        } else {
            ++unique_y;
            prev_y = sorted_y[i]->y;
            additional_y = ((sorted_y[i]->y + 1) - unique_y) * 999999;
            sorted_y[i]->y += additional_y;
        }

        if(prev_x == sorted_x[i]->x) {
            sorted_x[i]->x += additional_x;
        } else {
            ++unique_x;
            prev_x = sorted_x[i]->x;
            additional_x = ((sorted_x[i]->x + 1) - unique_x) * 999999;
            sorted_x[i]->x += additional_x;
        }
    }

    long long int sum = 0;
    for(int i = 0; i < galaxies_count; ++i)
        for(int j = i + 1; j < galaxies_count; ++j)
            sum += llabs(galaxies[i].x - galaxies[j].x) + llabs(galaxies[i].y - galaxies[j].y);

    return sum;
}

int main()
{
    char buff[COLS];
    int y = 0;
    while (fgets(buff, COLS, stdin) != NULL) {
        for(int x = 0; buff[x] != '\0' && buff[x] != '\n'; ++x) {
            if(buff[x] == '#') {
                galaxies = realloc(galaxies, sizeof(struct point) * ++galaxies_count);
                galaxies[galaxies_count - 1].y = y;
                galaxies[galaxies_count - 1].x = x;
            }
        }
        ++y;
    }

    // for(int i = 0; i < galaxies_count; ++i)
    //     printf("(%d, %d)\n", galaxies[i].y, galaxies[i].x);
    // printf("built\n");

    sort_y_x();
    
    // printf("%d\n", silver());
    
    printf("%lld\n", gold());

    return 0;
}