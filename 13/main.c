#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 1200
#define COLS 100

enum reflection {
    horizontal,
    vertical
};

struct pattern {
    int width;
    int height;
    enum reflection dir;
    int last_middle;
    char data[30][30];
};

struct pattern *patterns = NULL;
int patterns_count = 0;

struct pair {
    int silver;
    int gold;
};

struct pair solve() {
    struct pair pair = {0};

    for(int i = 0; i < patterns_count; ++i) {
        struct pattern *pattern = &patterns[i];
        int height = pattern->height;
        int width = pattern->width;

        int vleft_silver = 0;
        int vleft_gold = 0;
        for(int j = 1; j < width; ++j) {
            int gaps = (j < width - j) * j + (j >= width - j) * (width - j);
            int start = j - gaps;
            int end = j + (gaps - 1);

            int not_match = 0;
            for(int y = 0; y < height; ++y){
                int counter = 0;
                for(int x = start; x < j; ++x)
                    not_match += pattern->data[y][x] != pattern->data[y][end - counter++];
            }
            
            if(not_match == 0)
                vleft_silver = j;
            if(not_match == 1)
                vleft_gold = j;

            if(vleft_gold && vleft_silver)
                break;
        }

        int hup_silver = 0;
        int hup_gold = 0;
        for(int j = 1; j < height; ++j) {
            int gaps = (j < height - j) * j + (j >= height - j) * (height - j);
            int start = j - gaps;
            int end = j + (gaps - 1);

            int not_match = 0;
            int counter = 0;
            for(int y = start; y < j; ++y){
                for(int x = 0; x < width; ++x)
                    not_match += pattern->data[y][x] != pattern->data[end - counter][x];
                ++counter;
            }

            if(not_match == 0)
                hup_silver = j;
            if(not_match == 1)
                hup_gold = j;

            if(hup_gold && hup_silver)
                break;
        }

        pair.silver += vleft_silver + hup_silver * 100;
        pair.gold += vleft_gold + hup_gold * 100;
    }

    return pair;
}

int main()
{
    bool new_pattern = true;
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        struct pattern *pattern;

        if(new_pattern) {
            patterns = realloc(patterns, sizeof(struct pattern) * ++patterns_count);
            pattern = &patterns[patterns_count - 1];
            memset(pattern, 0, sizeof(struct pattern));
            new_pattern = false;
        }

        if(buff[0] == '\0' || buff[0] == '\n') {
            new_pattern = true;
            continue;
        }

        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            pattern->data[pattern->height][i] = buff[i];
        }

        pattern->width = i;
        ++pattern->height;
    }

    struct pair pair = solve();
    printf("%d\n", pair.silver);
    printf("%d\n", pair.gold);

    return 0;
}