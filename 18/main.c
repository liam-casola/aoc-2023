#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 800
#define COLS 30

enum DIRECTION { RIGHT, DOWN, LEFT, UP, NO_DIRECTION };

enum DIRECTION ctodir(const char c) {
    switch (c) {
    case 'U': return UP;
    case 'D': return DOWN;
    case 'L': return LEFT;
    case 'R': return RIGHT;
    default: return NO_DIRECTION;
    }
}

struct dig {
    enum DIRECTION dir;
    int y;
    int x;
    int length;
    uint32_t color;
};

struct dig_plan {
    int length;
    struct dig *digs;
};

struct dig_plan dig_plan = {0};

void move(enum DIRECTION dir, int length, int *y, int *x) {
    switch (dir) {
    case UP: *y -= length; break;
    case DOWN: *y += length; break;
    case LEFT: *x -= length; break;
    case RIGHT: *x += length; break;
    }
}

int silver() {
    int total_length = dig_plan.digs[0].length;
    //find start coordinate for each trench
    dig_plan.digs[0].y = 0;
    dig_plan.digs[0].x = 0;
    for(int i = 1; i < dig_plan.length; ++i) {
        struct dig *prev_dig = &dig_plan.digs[i - 1];
        int y = prev_dig->y;
        int x = prev_dig->x;
        move(prev_dig->dir, prev_dig->length, &y, &x);
        dig_plan.digs[i].y = y;
        dig_plan.digs[i].x = x;
        // printf("(%d,%d) %d %d %u\n", dig_plan.digs[i].y, dig_plan.digs[i].x, dig_plan.digs[i].dir, dig_plan.digs[i].length, dig_plan.digs[i].color);
        total_length += dig_plan.digs[i].length;
    }

    //duplicate first element to last place
    dig_plan.digs = realloc(dig_plan.digs, sizeof(struct dig) * ++dig_plan.length);
    dig_plan.digs[dig_plan.length - 1].dir = dig_plan.digs[0].dir;
    dig_plan.digs[dig_plan.length - 1].length = dig_plan.digs[0].length;
    dig_plan.digs[dig_plan.length - 1].color = dig_plan.digs[0].color;
    dig_plan.digs[dig_plan.length - 1].y = dig_plan.digs[0].y;
    dig_plan.digs[dig_plan.length - 1].x = dig_plan.digs[0].x;

    //calculate area
    int area = 0;

    for(int i = 0; i < dig_plan.length - 1; ++i) {
        // printf("%d %d %d %d\n", dig_plan.digs[i].y, dig_plan.digs[i + 1].x, dig_plan.digs[i].x, dig_plan.digs[i + 1].y);
        area += dig_plan.digs[i].y * dig_plan.digs[i + 1].x - dig_plan.digs[i].x * dig_plan.digs[i + 1].y;
    }

    area = abs(area);
    area += total_length + 2;
    area /= 2;

    return area;
}

union main
{
    /* data */
};


long long int gold() {
    //convert color to instruction
    for(int i = 0; i < dig_plan.length; ++i) {
        struct dig *dig = &dig_plan.digs[i];
        dig->dir = dig->color & 0xF;
        dig->length = dig->color >> 4;
        // printf("(%d,%d) %d %d %u\n", dig_plan.digs[i].y, dig_plan.digs[i].x, dig_plan.digs[i].dir, dig_plan.digs[i].length, dig_plan.digs[i].color);
    }

    int total_length = dig_plan.digs[0].length;
    //find start coordinate for each trench
    dig_plan.digs[0].y = 0;
    dig_plan.digs[0].x = 0;
    for(int i = 1; i < dig_plan.length; ++i) {
        struct dig *prev_dig = &dig_plan.digs[i - 1];
        int y = prev_dig->y;
        int x = prev_dig->x;
        move(prev_dig->dir, prev_dig->length, &y, &x);
        dig_plan.digs[i].y = y;
        dig_plan.digs[i].x = x;
        // printf("(%d,%d) %d %d %u\n", dig_plan.digs[i].y, dig_plan.digs[i].x, dig_plan.digs[i].dir, dig_plan.digs[i].length, dig_plan.digs[i].color);
        total_length += dig_plan.digs[i].length;
    }

    //duplicate first element to last place
    dig_plan.digs = realloc(dig_plan.digs, sizeof(struct dig) * ++dig_plan.length);
    dig_plan.digs[dig_plan.length - 1].dir = dig_plan.digs[0].dir;
    dig_plan.digs[dig_plan.length - 1].length = dig_plan.digs[0].length;
    dig_plan.digs[dig_plan.length - 1].color = dig_plan.digs[0].color;
    dig_plan.digs[dig_plan.length - 1].y = dig_plan.digs[0].y;
    dig_plan.digs[dig_plan.length - 1].x = dig_plan.digs[0].x;

    //calculate area
    long long int area = 0;

    for(int i = 0; i < dig_plan.length - 1; ++i) {
        // printf("%d %d %d %d\n", dig_plan.digs[i].y, dig_plan.digs[i + 1].x, dig_plan.digs[i].x, dig_plan.digs[i + 1].y);
        area += (long long)dig_plan.digs[i].y * (long long)dig_plan.digs[i + 1].x - (long long)dig_plan.digs[i].x * (long long)dig_plan.digs[i + 1].y;
    }

    area = llabs(area);
    area += (long long)total_length + 2;
    area /= 2;

    return area;
}

int main()
{
    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        dig_plan.digs = realloc(dig_plan.digs, sizeof(struct dig) * ++dig_plan.length);
        struct dig *dig = &dig_plan.digs[dig_plan.length - 1];

        dig->dir = ctodir(buff[0]);

        char *endptr;
        dig->length = strtoll(&buff[2], &endptr, 10);
        dig->color = strtoull(endptr + 3, NULL, 16);
    }

    printf("%d\n", silver());

    //remove duplicate element to re-add later
    --dig_plan.length;
    
    printf("%lld\n", gold());

    free(buff);
    return 0;
}