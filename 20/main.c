#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 100
#define COLS 100

enum MODULE_TYPE { BUTTON, BROADCAST, FLIP_FLOP, CONJUNCTION };

struct module {
    enum MODULE_TYPE type;
    char label[12];
    int length;
    char destinations[7][12];
};

struct module *modules;
int modules_length = 0;

int main()
{
    modules = malloc(sizeof(struct module) * 100);

    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        struct module *module = &modules[modules_length++];

        int i = 0;
        if(buff[0] == '%')
            module->type = FLIP_FLOP;
        else if(buff[0] == '&')
            module->type = CONJUNCTION;
        else {
            module->type = BROADCAST;
            i = 1;
        }

        char label[12];
        int label_counter = 0;
        while(buff[i] != ' ') {
            label[label_counter++] = buff[i];
            ++i;
        }
        label[label_counter] = '\0';
        memcpy(module->label, label, label_counter);

        i += 4;
        while(buff[i] != '\n' && buff[i] != '\0') {
            label_counter = 0;
            while(buff[i] != ',') {
                label[label_counter++] = buff[i];
                ++i;
            }
            label[label_counter] = '\0';
            memcpy(module->label, label, label_counter);
        }
    }

    printf("%d\n", silver());
    
    printf("%lld\n", gold());

    free(buff);
    return 0;
}