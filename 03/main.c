#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 200
#define COLS 400

char schematic[ROWS][COLS];
int width = 0;
int height = 0;

bool adjacent(int y, int x) {
    int ystart = y > 0 ? y - 1 : y;
    int yend = y < height - 1 ? y + 1 : y;
    int xstart = x > 0 ? x - 1 : x;
    int xend = x < width - 1 ? x + 1 : x;
    for(int dy = ystart; dy <= yend; ++dy) {
        for(int dx = xstart; dx <= xend; ++dx) {
            //skip center
            if(dy == y && dx == x)
                continue;

            if(schematic[dy][dx] != '.' && (schematic[dy][dx] < '0' || schematic[dy][dx] > '9'))
                return true;
        }
    }
    return false;
}

int get_number(int iy, int *ix, bool *adj_symbol) {
    int y = iy;
    int x = *ix;

    char num[11];
    int num_pos = 0;

    while(schematic[y][x] >= '0' && schematic[y][x] <= '9') {
        num[num_pos++] = schematic[y][x];
        //check around
        if(adjacent(y, x))
            *adj_symbol = true;
        
        //advance right
        ++x;
        if(x > width)
            break;
    }
    num[num_pos] = '\0';

    *ix = x;
    
    return atoi(num);
}

bool next_number(int y, int *x) {
    while(schematic[y][*x] < '0' || schematic[y][*x] > '9') {
        ++(*x);
        if(*x > width)
            return false;
    }

    return true;
}

int silver() {
    int sum = 0;

    for(int y = 0; y < height; ++y) {
        int x = 0;
        while(next_number(y, &x)) {
            bool adj_symbol = false;
            int val = get_number(y, &x, &adj_symbol);

            if(adj_symbol)
                sum += val;
        }
    }

    return sum;
}

int full_number(int y, int x) {
    while(x > 0 && schematic[y][x - 1] >= '0' && schematic[y][x - 1] <= '9')
        --x;

    char num[11];
    int num_pos = 0;

    while(schematic[y][x] >= '0' && schematic[y][x] <= '9') {
        num[num_pos++] = schematic[y][x];

        ++x;
        if(x > width)
            break;
    }
    num[num_pos] = '\0';

    return atoi(num);
}

bool is_gear(int y, int x, int *product) {
    int ystart = y > 0 ? y - 1 : y;
    int yend = y < height - 1 ? y + 1 : y;
    int xstart = x > 0 ? x - 1 : x;
    int xend = x < width - 1 ? x + 1 : x;

    *product = 1;
    int num_count = 0;
    for(int dy = ystart; dy <= yend; ++dy) {
        bool cont = false;
        for(int dx = xstart; dx <= xend; ++dx) {
            if(schematic[dy][dx] >= '0' && schematic[dy][dx] <= '9') {
                if(!cont) {
                    ++num_count;
                    *product *= full_number(dy, dx);
                    cont = true;
                }
            } else {
                //if cont was true then number just ended
                cont = false;
            }
        }
    }

    return num_count == 2;
}

int gold() {
    int sum = 0;

    for(int y = 0; y < height; ++y) {
        for(int x = 0; x < width; ++x) {
            if(schematic[y][x] == '*') {
                int product;
                if(is_gear(y, x, &product))
                    sum += product;
            }
        }
    }

    return sum;
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            schematic[height][i] = buff[i];
        }

        if(!width)
            width = i;
        ++height;
    }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}