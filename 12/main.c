#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 1200
#define COLS 100

struct condition_record {
    char *springs;
    int springs_count;
    int *damaged;
    int damaged_count;
};

struct records {
    struct condition_record *condition_records;
    int condition_records_count;
};

struct records records = {0};

bool valid_arrangement(struct condition_record *record) {
    int *segments = NULL;
    int segments_count = 0;
    char prev = 0;
    for(int i = 0; i < record->springs_count; ++i) {
        if(record->springs[i] == '#') {
            if(prev != '#') {
                segments = realloc(segments, sizeof(int) * ++segments_count);
                segments[segments_count - 1] = 1;
            } else {
                ++segments[segments_count - 1];
            }
        }
        prev = record->springs[i];
    }

    if(segments_count == record->damaged_count)
        for(int i = 0; i < segments_count; ++i)
            if(segments[i] != record->damaged[i])
                return false;

    return true;
}

int silver() {
    for(int i = 0; i < records.condition_records_count; ++i) {
        struct condition_record *record = &records.condition_records[i];
        char *springs = malloc(sizeof(int) * record->springs_count);
        memcpy(springs, record->springs, sizeof(int) * record->springs_count);

        struct condition_record tmp_record = *record;
        tmp_record.springs = springs;

        for(int j = 0; j < record->springs_count; ++j) {

        }
        free(springs);
    }
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        records.condition_records = realloc(records.condition_records, sizeof(struct condition_record) * ++records.condition_records_count);
        struct condition_record *record = &records.condition_records[records.condition_records_count - 1];
        memset(record, 0, sizeof(struct condition_record));

        int i = 0;
        for(; buff[i] != ' '; ++i) {
            record->springs = realloc(record->springs, sizeof(char) * ++record->springs_count);
            record->springs[record->springs_count - 1] = buff[i];
        }

        for(; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            if(buff[i] >= '0' && buff[i] <= '9') {
                char *endptr;
                long long int val = strtoll(&buff[i], &endptr, 10);

                record->damaged = realloc(record->damaged, sizeof(int) * ++record->damaged_count);
                record->damaged[record->damaged_count - 1] = val;

                i = endptr - buff;
            }
        }
    }

    for(int i = 0; i < records.condition_records_count; ++i) {
        for(int j = 0; j < records.condition_records[i].springs_count; ++j)
            printf("%c", records.condition_records[i].springs[j]);
        printf(" ");
        for(int j = 0; j < records.condition_records[i].damaged_count; ++j)
            printf("%d,", records.condition_records[i].damaged[j]);
        printf("\n");
    }
    
    printf("%d\n", silver());
    
    // printf("%lld\n", gold());

    return 0;
}