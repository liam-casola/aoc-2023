#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 110
#define COLS 110

struct platform {
    int height;
    int width;
    char data[110][110];
};

struct platform platform = {0};

void print_platform() {
    for(int y = 0; y < platform.height; ++y) {
        for(int x = 0; x < platform.width; ++x)
            printf("%c", platform.data[y][x]);
        printf("\n");
    }
}

enum DIR {
    NORTH = 0,
    WEST = 1,
    SOUTH = 2,
    EAST = 3,
};

void advance_rock(enum DIR dir, int y, int x) {
    if(platform.data[y][x] != 'O')
        return;
    
    int y1 = y;
    int x1 = x;

    switch (dir) {
    case NORTH: --y1; break;
    case SOUTH: ++y1; break;
    case EAST: ++x1; break;
    case WEST: --x1; break;
    }

    if(y1 < 0 || y1 >= platform.height || x1 < 0 || x1 >= platform.width)
        return;

    if(platform.data[y1][x1] == '.'){
        platform.data[y1][x1] = 'O';
        platform.data[y][x] = '.';
    }
}

void settle_platform(enum DIR dir) {
    struct platform prev = {0};

    while(memcmp(&platform, &prev, sizeof(struct platform))) {
        memcpy(&prev, &platform, sizeof(struct platform));
        for(int y = 0; y < platform.height; ++y)
            for(int x = 0; x < platform.width; ++x)
                advance_rock(dir, y, x);
    }
}

int northern_load() {
    int load = 0;
     for(int y = 0; y < platform.height; ++y)
            for(int x = 0; x < platform.width; ++x)
                if(platform.data[y][x] == 'O')
                    load += platform.height - y;


    return load;
}

int silver() {
    settle_platform(NORTH);
    return northern_load();
}

int gold() {
    struct platform *history = malloc(sizeof(struct platform) * 1200);
    int history_count = 0;

    int cycle_length = 0;
    int offset = 0;
    int offset_idx = 0;

    int dir = NORTH;
    for(int i = 0; i < 1200; ++i) {
        settle_platform(dir++);
        // print_platform();
        // printf("\n");
        for(int j = 0; j < history_count; ++j) {
            int cmp = memcmp(&platform, &history[j], sizeof(struct platform));
            if(!cycle_length && cmp == 0 && offset_idx == j)
                cycle_length = i - offset;

            if(!offset && cmp == 0 && i % 4 == 0) {
                offset = i;
                offset_idx = j;
                // printf("%d iter %d history %d\n", dir == 0, i, j);
                break;
            }
            
        }
        memcpy(&history[history_count++], &platform, sizeof(struct platform));

        if(dir > EAST)
            dir = NORTH;
    }

    int additional_steps = (1000000000 - (offset / 4)) % cycle_length;

    memcpy(&platform, &history[offset + additional_steps * 4 - 1], sizeof(struct platform));

    printf("%d %d %d\n", offset, cycle_length, additional_steps);

    return northern_load();
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            platform.data[platform.height][i] = buff[i];
        }
        platform.width = i;
        ++platform.height;
    }

    struct platform tmp;
    memcpy(&tmp, &platform, sizeof(struct platform));

    printf("%d\n", silver());

    memcpy(&platform, &tmp, sizeof(struct platform));
    
    printf("%d\n", gold());

    return 0;
}