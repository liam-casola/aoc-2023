#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define lbuff 2000 //columns
#define ldata lbuff //rows


char data[ldata][lbuff];
size_t data_count = 0;

int silver() {
    int values[2000];
    int value_count = 0;

    for(int row = 0; row < data_count; ++row) {
        signed char digits[3] = {-1, -1, '\0'};

        for(int i = 0; data[row][i] != '\n' && data[row][i] != '\0'; ++i) {
            if(data[row][i] >= '0' && data[row][i] <= '9') {
                if(digits[0] > 0) {
                    digits[1] = data[row][i];
                } else {
                    digits[0] = data[row][i];
                }
            }
        }

        if(digits[1] < 0)
            digits[1] = digits[0];

        values[value_count] = atoi(digits);
        ++value_count;
    }

    int sum = 0;
    for(int i = 0; i < value_count; ++i)
        sum += values[i];

    return sum;
}

static char *strrstr(const char *haystack, const char *needle)
{
    if (*needle == '\0')
        return (char *) haystack;

    char *result = NULL;
    for (;;) {
        char *p = strstr(haystack, needle);
        if (p == NULL)
            break;
        result = p;
        haystack = p + 1;
    }

    return result;
}

int gold() {
    char word_digits[9][6] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

    int values[2000];
    int value_count = 0;

    for(int row = 0; row < data_count; ++row) {
        signed char digits[3] = {-1, -1, '\0'};
        int first_digit_pos = lbuff;
        int last_digit_pos = -1;

        //first numeral
        for(char c = '0'; c <= '9'; ++c) {
            char *ptr = strchr(data[row], c);
            if(!ptr)
                continue;
            char pos = ptr - data[row];
            if(pos < first_digit_pos) {
                first_digit_pos = pos;
                digits[0] = c;
            }
        }

        //first word
        for(int i = 0; i < sizeof(word_digits) / sizeof(word_digits[0]); ++i) {
            char *ptr = strstr(data[row], word_digits[i]);
            if(!ptr)
                continue;
            char pos = ptr - data[row];
            if( pos < first_digit_pos) {
                first_digit_pos = pos;
                digits[0] = '1' + i;
            }
        }

        //last numeral
        for(char c = '0'; c <= '9'; ++c) {
            char *ptr = strrchr(data[row], c);
            if(!ptr)
                continue;
            char pos = ptr - data[row];
            if(pos > last_digit_pos) {
                last_digit_pos = pos;
                digits[1] = c;
            }
        }

        //last word
        for(int i = 0; i < sizeof(word_digits) / sizeof(word_digits[0]); ++i) {
            char *ptr = strrstr(data[row], word_digits[i]);
            if(!ptr)
                continue;
            char pos = ptr - data[row];
            if( pos > last_digit_pos) {
                last_digit_pos = pos;
                digits[1] = '1' + i;
            }
        }

        if(digits[1] < 0)
            digits[1] = digits[0];

        values[value_count] = atoi(digits);
        ++value_count;
    }

    int sum = 0;
    for(int i = 0; i < value_count; ++i)
        sum += values[i];

    return sum;
}

int main()
{
    while (fgets(data[data_count], lbuff, stdin) != NULL) {
        ++data_count;
    }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}