#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 110
#define COLS 24000

char *initseq;
int initseq_count = 0;

struct lens {
    char label[20];
    int focal_length;
};

struct box {
    struct lens lenses[200];
    int lenses_count;
};

struct box boxes[256] = {0};

int silver() {
    int sum = 0;

    int hash = 0;
    for(int i = 0; i < initseq_count; ++i) {
        if(initseq[i] == ',') {
            sum += hash;
            hash = 0;
            continue;
        }

        hash += initseq[i];
        hash *= 17;
        hash %= 256;
    }
    sum += hash;

    return sum;
}

void add_lens(int hash, char *label, int focal_length) {
    for(int i = 0; i < boxes[hash].lenses_count; ++i) {
        if(!strcmp(boxes[hash].lenses[i].label, label)) {
            boxes[hash].lenses[i].focal_length = focal_length;
            return;
        }
    }

    strcpy(boxes[hash].lenses[boxes[hash].lenses_count].label, label);
    boxes[hash].lenses[boxes[hash].lenses_count].focal_length = focal_length;
    boxes[hash].lenses_count++;
}

void remove_lens(int hash, char *label) {
    for(int i = 0; i < boxes[hash].lenses_count; ++i) {
        if(!strcmp(boxes[hash].lenses[i].label, label)) {
            boxes[hash].lenses_count--;
            for(; i < boxes[hash].lenses_count; ++i) {
                strcpy(boxes[hash].lenses[i].label, boxes[hash].lenses[i + 1].label);
                boxes[hash].lenses[i].focal_length = boxes[hash].lenses[i + 1].focal_length;
            }
            return;
        }
    }
}

void print_boxes() {
    for(int i = 0; i < 256; ++i) {
        if(boxes[i].lenses_count == 0)
            continue;
        printf("box %d: ", i);
        for(int j = 0; j < boxes[i].lenses_count; ++j) {
            printf("[%s %d] ", boxes[i].lenses[j].label, boxes[i].lenses[j].focal_length);
        }
        printf("\n");
    }
}

int gold() {
    int hash = 0;
    char label[20];
    int label_count = 0;
    int focal_length = 0;
    bool adding = true;
    for(int i = 0; i < initseq_count; ++i) {
        if(initseq[i] == ',') {
            label[label_count] = '\0';
            // printf("%d %s %d\n", hash, label, focal_length);
            if(adding)
                add_lens(hash, label, focal_length);
            else
                remove_lens(hash, label);
            // print_boxes();
            
            label_count = 0;
            focal_length = 0;
            hash = 0;
            continue;
        }

        if(initseq[i] >= 'a' && initseq[i] <= 'z') {
            label[label_count++] = initseq[i];
            hash += initseq[i];
            hash *= 17;
            hash %= 256;
        }

        if(initseq[i] == '=')
            adding = true;
        if(initseq[i] == '-')
            adding = false;

        if(initseq[i] >= '0' && initseq[i] <= '9') {
            focal_length = initseq[i] - '0';
        }
    }
    label[label_count] = '\0';
    // printf("%d %s %d\n", hash, label, focal_length);
    if(adding)
        add_lens(hash, label, focal_length);
    else
        remove_lens(hash, label);

    int sum = 0;
    for(int i = 0; i < 256; ++i) {
        int focusing_power = 0;
        for(int j = 0; j < boxes[i].lenses_count; ++j) {
            focusing_power += (i + 1) * (j + 1) * boxes[i].lenses[j].focal_length;
        }
        sum += focusing_power;
    }

    return sum;
}

int main()
{
    initseq = malloc(COLS);

    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            initseq[i] = buff[i];
        }
        initseq_count = i;
    }

    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}