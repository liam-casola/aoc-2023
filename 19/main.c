#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 1000
#define COLS 100

//0 = x, 1 = m, 2 = a, 3 = s
struct rule {
    int category;
    char op;
    int value;
    char label[5];
};

struct workflow {
    char label[5];
    int length;
    struct rule rules[5];
};

struct part {
    int values[4];
};

int workflows_length = 0;
struct workflow *workflows;

int parts_length = 0;
struct part *parts;

int ctocategory(char c) {
    switch (c){
    case 'x': return 0;
    case 'm': return 1;
    case 'a': return 2;
    case 's': return 3;
    default: return -1; //bad category, probably last rule
    }
}

int find_workflow(char *label) {
    for(int i = 0; i < workflows_length; ++i) {
        if(strcmp(workflows[i].label, label) == 0)
            return i;
    }
}

char * process_workflow(int idx, struct part *part) {
    struct workflow *workflow = &workflows[idx];
    for(int i = 0; i < workflow->length; ++i) {
        if(i == workflow->length - 1) {
            //final rule
            return workflow->rules[i].label;
        } else {
            int value = part->values[workflow->rules[i].category];
            if(workflow->rules[i].op == '>')
                if(value > workflow->rules[i].value)
                    return workflow->rules[i].label;
            if(workflow->rules[i].op == '<')
                if(value < workflow->rules[i].value)
                    return workflow->rules[i].label;
        }
    }
}

int silver() {
    // for(int i = 0; i < workflows_length; ++i) {
    //     printf("%s{", workflows[i].label);
    //     for(int j = 0; j < workflows[i].length; ++j)
    //         printf("%d%c%d:%s,", workflows[i].rules[j].category, workflows[i].rules[j].op, workflows[i].rules[j].value, workflows[i].rules[j].label);
    //     printf("}\n");
    // }

    // for(int i = 0; i < parts_length; ++i) {
    //     for(int j = 0; j < 4; ++j)
    //         printf("%d ", parts[i].values[j]);
    //     printf("\n");
    // }

    char in_workflow[] = "in";

    int sum = 0;
    for(int i = 0; i < parts_length; ++i) {
        char *result = in_workflow;
        while(1) {
            if(strcmp(result, "A") == 0) {
                for(int j = 0; j < 4; ++j)
                    sum += parts[i].values[j];
                break;
            }

            if(strcmp(result, "R") == 0)
                break;

            int workflow = find_workflow(result);
            // printf("part: %d wf: %d %s\n", i, workflow, result);
            result = process_workflow(workflow, &parts[i]);
        }
    }

    return sum;
}

long long int gold() {
    

    return 0;
}

int main()
{
    workflows = malloc(sizeof(struct workflow) * 1000);
    memset(workflows, 0, sizeof(struct workflow) * 1000);
    parts = malloc(sizeof(struct part) * 500);

    bool parsing_workflows = true;
    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        if(buff[0] == '\n'){
            parsing_workflows = false;
            continue;
        }

        if(parsing_workflows) {
            //parse workflows
            struct workflow *workflow = &workflows[workflows_length++];
            int i = 0;
            //label
            while(buff[i++] != '{') {}
            memcpy(workflow->label, &buff[0], i - 1);
            workflow->label[i - 1] = '\0';

            //rules
            while(1) {
                struct rule *rule = &workflow->rules[workflow->length++];
                int category;
                if((category = ctocategory(buff[i])) >= 0 && (buff[i + 1] == '<' || buff[i + 1] == '>')) {
                    rule->category = category;
                    rule->op = buff[++i];
                    char *endptr;
                    rule->value = strtoll(&buff[++i], &endptr, 10);
                    i = (endptr - buff) + 1;
                    while(buff[i++] != ',') {}
                    memcpy(rule->label, &buff[(endptr - buff) + 1], (i - 1) - ((endptr - buff) + 1));
                    // printf("%d\n", (i - 1) - ((endptr - buff) + 1));
                    rule->label[(i - 1) - ((endptr - buff) + 1)] = '\0';
                } else {
                    //last rule, label only
                    int start = i;
                    while(buff[i++] != '}') {}
                    memcpy(rule->label, &buff[start], (i - 1) - start);
                    // printf("%d\n", (i - 1) - start);
                    rule->label[(i - 1) - start] = '\0';
                    break;
                }
            }
        } else {
            //parse parts
            struct part *part = &parts[parts_length++];
            int category = 0;
            int i = 0;
            while(category < 4) {
                if(buff[i] >= '0' && buff[i] <= '9') {
                    char *endptr;
                    part->values[category++] = strtoll(&buff[i], &endptr, 10);
                    i = endptr - buff;
                } else {
                    ++i;
                }
            }
        }
    }

    printf("%d\n", silver());
    
    printf("%lld\n", gold());

    free(buff);
    return 0;
}