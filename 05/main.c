#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 400
#define COLS 400

struct range_map {
    long long int dst;
    long long int src;
    long long int range;
};

struct maps {
    int maps_count;
    struct range_map *maps;
};

struct almanac {
    int seeds_count;
    long long int *seeds;
    int maps_count;
    struct maps *maps;
};

struct almanac alm = {0};

long long int silver() {
    long long int lowest = LONG_LONG_MAX;

    //for each seed
    for(int s = 0; s < alm.seeds_count; ++s) {
        //descend the maps
        long long int prev = alm.seeds[s];
        for(int i = 0; i < alm.maps_count; ++i) {
            for(int j = 0; j < alm.maps[i].maps_count; ++j) {
                struct range_map *rmap = &(alm.maps[i].maps[j]);
                if(prev >= rmap->src && prev < rmap->src + rmap->range) {
                    // printf("s%d %d: %lld -> ", s, i, prev);
                    prev = rmap->dst + (prev - rmap->src);
                    // printf("%lld\n", prev);
                    break;
                }
            }
        }

        if(prev < lowest)
            lowest = prev;
    }

    return lowest;
}

/* ToDo: this is a brute force, figure out how to do it better with seed ranges */
long long int gold() {
    long long int lowest = LONG_LONG_MAX;

    //for each initial seed
    for(int s = 0; s < alm.seeds_count; s+=2) {
        for(int sr = 0; sr < alm.seeds[s+1]; ++sr) {
            //descend the maps
            long long int prev = alm.seeds[s] + sr;
            for(int i = 0; i < alm.maps_count; ++i) {
                for(int j = 0; j < alm.maps[i].maps_count; ++j) {
                    struct range_map *rmap = &(alm.maps[i].maps[j]);
                    if(prev >= rmap->src && prev < rmap->src + rmap->range) {
                        // printf("s%d %d: %lld -> ", s, i, prev);
                        prev = rmap->dst + (prev - rmap->src);
                        // printf("%lld\n", prev);
                        break;
                    }
                }
            }

            if(prev < lowest)
                lowest = prev;
        }
    }

    return lowest;
}

int main()
{

    char buff[COLS];
    fgets(buff, COLS, stdin); //seeds
    for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
        while(buff[i] < '0' || buff[i] > '9') //arrive at next number
            ++i;

        char *endptr;
        long long int val = strtoll(&buff[i], &endptr, 10);

        alm.seeds = realloc(alm.seeds, sizeof(long long int) * ++alm.seeds_count);
        alm.seeds[alm.seeds_count - 1] = val;

        i = endptr - buff;
    }

    while (fgets(buff, COLS, stdin) != NULL) {
        struct range_map *cur_map_ptr = NULL;
        int stage = 0;
        for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            if(buff[i] == ':') {
                alm.maps = realloc(alm.maps, sizeof(struct maps) * ++alm.maps_count);
                alm.maps[alm.maps_count - 1].maps_count = 0;
                alm.maps[alm.maps_count - 1].maps = NULL;
            }
            
            if(buff[i] >= '0' && buff[i] <= '9') {
                char *endptr;
                long long int val = strtoll(&buff[i], &endptr, 10);

                if(!cur_map_ptr) {
                    alm.maps[alm.maps_count - 1].maps = realloc(alm.maps[alm.maps_count - 1].maps, sizeof(struct range_map) * ++alm.maps[alm.maps_count - 1].maps_count);
                    cur_map_ptr = &(alm.maps[alm.maps_count - 1].maps[alm.maps[alm.maps_count-1].maps_count - 1]);
                }

                if(stage == 0)
                    cur_map_ptr->dst = val;
                if(stage == 1)
                    cur_map_ptr->src = val;
                if(stage == 2)
                    cur_map_ptr->range = val;

                ++stage;

                i = endptr - buff;
            }
        }
    }

    // for(int i = 0; i < alm.seeds_count; ++i) {
    //     printf("%lld ", alm.seeds[i]);
    // }

    // for(int i = 0; i < alm.maps_count; ++i) {
    //     printf("\nnew map\n");
    //     for(int j = 0; j < alm.maps[i].maps_count; ++j) {
    //         printf("%lld %lld %lld\n", alm.maps[i].maps[j].dst, alm.maps[i].maps[j].src, alm.maps[i].maps[j].range);
    //     }
    // }
    
    printf("%lld\n", silver());
    
    printf("%d\n", gold());

    return 0;
}