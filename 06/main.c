#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
 
#define ROWS 400
#define COLS 400

struct race {
    int duration;
    int record;
};

struct race *races = NULL;
int races_count = 0;

long long int duration;
long long int record;

int silver() {
    int product = 1;

    for(int i = 0; i < races_count; ++i) {
        int ways_to_win = 0;
        for(int j = 0; j <= races[i].duration; ++j) {
            // printf("%d\n", races[i].duration - j);
            if(races[i].record < (races[i].duration - j) * j)
                ++ways_to_win;
        }
        product *= ways_to_win;
    }

    return product;
}

int silver2() {
    int product = 1;

    for(int i = 0; i < races_count; ++i) {
        double dlower = races[i].duration / 2.0 -  sqrt(races[i].duration * races[i].duration - 4 * races[i].record) / 2.0;
        double dupper = races[i].duration / 2.0 +  sqrt(races[i].duration * races[i].duration - 4 * races[i].record) / 2.0;
        int lower = ceil(dlower);
        int upper = floor(dupper);

        if(dlower == lower)
            ++lower;
        if(dupper == upper)
            --upper;

        product *= (upper - lower + 1);
    }

    return product;
}

long long int gold() {
    long long int ways_to_win = 0;
    for(long long int i = 0; i <= duration; ++i) {
        if(record < (duration - i) * i)
            ++ways_to_win;
    }

    return ways_to_win;
}

long long int gold2() {
    double dlower = duration / 2.0 -  sqrt(duration * duration - 4 * record) / 2.0;
    double dupper = duration / 2.0 +  sqrt(duration * duration - 4 * record) / 2.0;
    long long int lower = ceil(dlower);
    long long int upper = floor(dupper);

    if(dlower == lower)
        ++lower;
    if(dupper == upper)
        --upper;

    return (upper - lower + 1);
}

int main()
{
    char buff[COLS];
    //time
    fgets(buff, COLS, stdin);
    for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
        if(buff[i] >= '0' && buff[i] <= '9') {
            char *endptr;
            int val = strtoll(&buff[i], &endptr, 10);

            races = realloc(races, sizeof(struct race) * ++races_count);
            races[races_count - 1].duration = val;

            i = endptr - buff;
        }
    }

    char tmpval[30];
    int tmpval_length = 0;
    for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i)
        if(buff[i] >= '0' && buff[i] <= '9')
            tmpval[tmpval_length++] = buff[i];
    tmpval[tmpval_length] = '\0';
    duration = strtoll(tmpval, NULL, 10);

    //distance
    fgets(buff, COLS, stdin);
    int idx = 0;
    for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
        if(buff[i] >= '0' && buff[i] <= '9') {
            char *endptr;
            int val = strtoll(&buff[i], &endptr, 10);

            races[idx++].record = val;

            i = endptr - buff;
        }
    }

    tmpval_length = 0;
    for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i)
        if(buff[i] >= '0' && buff[i] <= '9')
            tmpval[tmpval_length++] = buff[i];
    tmpval[tmpval_length] = '\0';
    record = strtoll(tmpval, NULL, 10);

    // for(int i = 0; i < races_count; ++i)
    //     printf("%d %d\n", races[i].duration, races[i].record);
    
    printf("%d\n", silver());

    printf("%d\n", silver2());
    
    printf("%lld\n", gold());

    printf("%lld\n", gold2());

    return 0;
}