#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 400
#define COLS 400

struct history {
    int values_count;
    int **diffs;
    int diffs_count;
};

struct history *histories = NULL;
int histories_count = 0;

bool diff_zero(int *diff, int length) {
    bool zero = false;
    for(int i = 0; i < length; ++i)
        zero |= diff[i];
    return !zero;
}

void build_diffs(struct history *history) {
    int diffs_count = 1;
    int *diff = history->diffs[0];
    while(!diff_zero(diff, history->values_count - (diffs_count - 1))) {
        history->diffs = realloc(history->diffs, sizeof(int *) * ++diffs_count);
        history->diffs[diffs_count - 1] = malloc(sizeof(int) * (history->values_count - (diffs_count - 1)));
        diff = history->diffs[diffs_count - 1];
        int *prev_diff = history->diffs[diffs_count - 2];

        int prev = prev_diff[0];
        for(int i = 1; i < history->values_count - (diffs_count - 2); ++i) {
            diff[i - 1] = prev_diff[i] - prev;
            prev = prev_diff[i];
        }
    }
    history->diffs_count = diffs_count;
}

int silver() {
    int next = 0;

    for(int i = 0; i < histories_count; ++i) {
        int diffs_count = 1;
        int *diff = histories[i].diffs[diffs_count - 1];
        while(!diff_zero(diff, histories[i].values_count - (diffs_count - 1))) {
            next += histories[i].diffs[diffs_count - 1][histories[i].values_count - diffs_count];
            diff = histories[i].diffs[++diffs_count - 1];
        }
    }
    
    return next;
}

int gold() {
    int sum = 0;

    for(int i = 0; i < histories_count; ++i) {
        int prev = 0;
        for(int j = histories[i].diffs_count - 1; j >= 0; --j) {
            prev = histories[i].diffs[j][0] - prev;
        }
        sum += prev;
    }

    return sum;
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        histories = realloc(histories, sizeof(struct history) * ++histories_count);
        struct history *history = &histories[histories_count - 1];
        history->values_count = 0;
        history->diffs = malloc(sizeof(int *) * 1);
        history->diffs[0] = NULL;

        for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            char *endptr;
            long long int val = strtoll(&buff[i], &endptr, 10);

            history->diffs[0] = realloc(history->diffs[0], sizeof(int) * ++history->values_count);
            history->diffs[0][history->values_count - 1] = val;

            i = endptr - buff;
        }
    }

    for(int i = 0; i < histories_count; ++i) {
        build_diffs(&histories[i]);
    }

    // for(int i = 0; i < histories_count; ++i) {
    //     for(int j = 0; j < histories[i].values_count; ++j)
    //         printf("%d ", histories[i].diffs[0][j]);
    //     printf("\n");
    // }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}