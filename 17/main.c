#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 150
#define COLS 150

struct grid {
    int height;
    int width;
    short data[COLS][ROWS];
    int heat[COLS][ROWS][4][3];
};

struct grid *grid;

enum DIRECTION { UP, DOWN, LEFT, RIGHT, DIRECTION_END };

bool move(enum DIRECTION dir, int *y, int *x) {
    int y1 = *y;
    int x1 = *x;
    switch (dir) {
        case UP: --y1; break;
        case DOWN: ++y1; break;
        case LEFT: --x1; break;
        case RIGHT: ++x1; break;
    }

    if(y1 < 0 || y1 >= grid->height || x1 < 0 || x1 >= grid->width)
        return false;

    // printf("y:%d x:%d\n", y1, x1);

    *y = y1;
    *x = x1;
    return true;
}

struct dijkstra_data {
    enum DIRECTION dir;
    int dir_depth;
    int y;
    int x;
    int priority;
};

struct dijkstra_data *data_arr;
int data_arr_count = 0;

void dijkstra(enum DIRECTION dir, int dir_depth, int y, int x, int pirority) {
    int heat = 0;
    if(dir < DIRECTION_END)
        heat = grid->heat[y][x][dir][dir_depth];
    for(int i = 0; i < DIRECTION_END; ++i) {
        //prevent reverse
        if(dir == UP && i == DOWN) continue;
        if(dir == DOWN && i == UP) continue;
        if(dir == LEFT && i == RIGHT) continue;
        if(dir == RIGHT && i == LEFT) continue;
        int y1 = y;
        int x1 = x;
        int dir_depth1 = dir_depth;
        // if(y == 2 && x == 4 && dir == RIGHT)
        //     printf("interest:\n");
        if(((i == dir && dir_depth1 < 2) || i != dir) && move(i, &y1, &x1)) {
            if(i == dir)
                dir_depth1++;
            else
                dir_depth1 = 0;
            // printf("depth %d (%d %d)\n", dir_depth1, y1, x1);
            if(grid->heat[y1][x1][i][dir_depth1] > heat + grid->data[y1][x1]) {
                grid->heat[y1][x1][i][dir_depth1] = heat + grid->data[y1][x1];
                // printf("=depth %d (%d %d) %d %d\n", dir_depth1, y1, x1, heat, grid->data[y1][x1]);
                // dijkstra(i, dir_depth, y, x);
                struct dijkstra_data data = {i, dir_depth1, y1, x1, pirority + 1};
                data_arr[data_arr_count++] = data;
            }
        }
    }
}

// void dijkstra(enum DIRECTION dir, int dir_depth, int y, int x) {
//     int heat = grid->heat[y][x][dir_depth];
//     for(int i = 0; i < DIRECTION_END; ++i) {
//         int y0 = y;
//         int x0 = x;
//         int dir_depth0 = dir_depth;
//         if(((i == dir && dir_depth < 2) || i != dir) && move(i, &y, &x)) {
//             if(i == dir)
//                 dir_depth++;
//             else
//                 dir_depth = 0;
//             if(grid->heat[y][x][dir_depth] > heat + grid->data[y][x]) {
//                 grid->heat[y][x][dir_depth] = heat + grid->data[y][x];
//                 dijkstra(i, dir_depth, y, x);
//             }
//             y = y0;
//             x = x0;
//             dir_depth = dir_depth0;
//         }
//     }
// }

int silver() {
    for(int y = 0; y < grid->height; ++y)
        for(int x = 0; x < grid->width; ++x)
            for(int dir = 0; dir < 4; ++dir)
                for(int d = 0; d < 3; ++d)
                    grid->heat[y][x][dir][d] = INT_MAX;
    // for(int dir = 0; dir < 4; ++dir)
    //     grid->heat[0][0][dir][0] = 0;

    // dijkstra(DIRECTION_END, 0, 0, 0);
    struct dijkstra_data data = {DIRECTION_END, 0, 0, 0, 0};
    data_arr[data_arr_count++] = data;
    int counter = 0;
    int highest_priority_idx = 0;
    while(1) {
        highest_priority_idx = -1;
        int highest_priority = -1;
        if(counter % 10000000 == 0)
            printf("%d in arr\n", data_arr_count);
        for(int i = 0; i < data_arr_count; ++i)
            if(data_arr[i].priority > highest_priority) {
                highest_priority = data_arr[i].priority;
                highest_priority_idx = i;
            }

        if(highest_priority_idx < 0)
            break;

        struct dijkstra_data data = data_arr[highest_priority_idx];
        dijkstra(data.dir, data.dir_depth, data.y, data.x, data.priority);
        data_arr[highest_priority_idx].priority = -1;

        ++counter;
    }

    // for(int d = 0; d < 3; ++d){
    //     printf("depth: %d\n", d);
    //     for(int y = 0; y < grid->height; ++y) {
    //         for(int x = 0; x < grid->width; ++x)
    //             printf("%d ", grid->heat[y][x][d]);
    //         printf("\n");
    //     }
    // }
    int min = INT_MAX;
    for(int dir = 0; dir < 4; ++dir)
        for(int d = 0; d < 3; ++d)
            if(grid->heat[grid->height - 1][grid->width - 1][dir][d] < min)
                min = grid->heat[grid->height - 1][grid->width - 1][dir][d];
            // printf("%d:%d:%d\n", dir, d, grid->heat[grid->height - 1][grid->width - 1][dir][d]);

    return min;
}

int main()
{
    grid = malloc(sizeof(struct grid));
    memset(grid, 0, sizeof(struct grid));

    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            grid->data[grid->height][i] = buff[i] - '0';
        }
        grid->width = i;
        grid->height++;
    }

    data_arr = malloc(sizeof(struct dijkstra_data) * 500000);

    printf("%d\n", silver());
    
    // printf("%d\n", gold());

    return 0;
}