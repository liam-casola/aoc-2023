#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 150
#define COLS 150

struct loop {
    char pipes[150][150];
    int distances[150][150];
    int width;
    int height;
    int start_x;
    int start_y;
};

struct loop loop = {0};

void print_pipes() {
    for(int y = 0; y < loop.height; ++y) {
        for(int x = 0; x < loop.width; ++x)
            printf("%c", loop.pipes[y][x]);
        printf("\n");
    }
}

void print_distances() {
    for(int y = 0; y < loop.height; ++y) {
        for(int x = 0; x < loop.width; ++x)
            printf("%d ", loop.distances[y][x]);
        printf("\n");
    }
}

char *dir_pipes[] = {
    "|LJ", //north
    "|7F", //south
    "-LF", //east
    "-J7"  //west
};
char dirs[4] = "NSEW";

//get the pipe in the specified direction from the current coordinate
//0 = no pipe in that direction
char get_pipe(char dir, int y, int x) {
    if(dir == 'N') --y;
    if(dir == 'S') ++y;
    if(dir == 'E') ++x;
    if(dir == 'W') --x;
    if(y < 0 || y >= loop.height || x < 0 || x >= loop.width)
        return 0;

    return loop.pipes[y][x];
}

bool pipe_connects(char dir, char pipe) {
    bool connects = false;
    for(int i = 0; i < sizeof(dirs); ++i)
        if(dir == dirs[i] && strchr(dir_pipes[i], pipe)) connects = true;
    return connects;
}

bool pipes_connected(char dir, int y, int x) {
    char pipe = loop.pipes[y][x];
    bool connected = pipe_connects(dir, pipe);
    pipe = get_pipe(dir, y, x);
    //invert direction for dst check
    switch (dir)
    {
    case 'N': dir = 'S'; break;
    case 'S': dir = 'N'; break;
    case 'E': dir = 'W'; break;
    case 'W': dir = 'E'; break;
    default: break;
    }
    connected &= pipe_connects(dir, pipe);

    return connected;
}

void convert_S() {
    char pipe;
    char connected_dirs[4] = {0};

    if(pipe = get_pipe('S', loop.start_y, loop.start_x))
        if(pipe_connects('N', pipe))
            connected_dirs[1] = 1; //connects from south to S
    if(pipe = get_pipe('N', loop.start_y, loop.start_x))
        if(pipe_connects('S', pipe))
            connected_dirs[0] = 1; //connects from north to S
    if(pipe = get_pipe('W', loop.start_y, loop.start_x))
        if(pipe_connects('E', pipe))
            connected_dirs[3] = 1; //connects from east to S
    if(pipe = get_pipe('E', loop.start_y, loop.start_x))
        if(pipe_connects('W', pipe))
            connected_dirs[2] = 1; //connects from west to S

    char all_char[SCHAR_MAX+1] = {0};
    for(int i = 0; i < sizeof(dirs); ++i)
         if(connected_dirs[i]) {
             char *str = dir_pipes[i];
             while(*str != '\0') {
                all_char[*str++]++;
             }
         }
        
    char common; 
    for(int i = 0; i < sizeof(all_char); ++i)
        if(all_char[i] == 2) {
            common = i;
            break;
        }

    loop.pipes[loop.start_y][loop.start_x] = common;
}

void dijkstra(int y, int x) {
    if(pipes_connected('N', y, x) && loop.distances[y][x] + 1 < loop.distances[y - 1][x]) {
        loop.distances[y - 1][x] = loop.distances[y][x] + 1;
        dijkstra(y - 1, x);
    }
    if(pipes_connected('S', y, x) && loop.distances[y][x] + 1 < loop.distances[y + 1][x]) {
        loop.distances[y + 1][x] = loop.distances[y][x] + 1;
        dijkstra(y + 1, x);
    }
    if(pipes_connected('E', y, x) && loop.distances[y][x] + 1 < loop.distances[y][x + 1]) {
        loop.distances[y][x + 1] = loop.distances[y][x] + 1;
        dijkstra(y, x + 1);
    }
    if(pipes_connected('W', y, x) && loop.distances[y][x] + 1 < loop.distances[y][x - 1]) {
        loop.distances[y][x - 1] = loop.distances[y][x] + 1;
        dijkstra(y, x - 1);
    }
}

int silver() {
    // print_pipes();

    //set distances to ~inf
    for(int y = 0; y < loop.height; ++y)
        for(int x = 0; x < loop.width; ++x)
            loop.distances[y][x] = INT_MAX;
    loop.distances[loop.start_y][loop.start_x] = 0;

    convert_S();

    // print_pipes();

    dijkstra(loop.start_y, loop.start_x);

    // print_distances();

    int farthest = 0;
    for(int y = 0; y < loop.height; ++y)
        for(int x = 0; x < loop.width; ++x)
            if(loop.distances[y][x] != INT_MAX)
                if(loop.distances[y][x] > farthest)
                    farthest = loop.distances[y][x];

    return farthest;
}

struct loop_piece {
    char pipe;
    int y;
    int x;
};

struct loop_piece *loop_arr = NULL;
int loop_arr_length = 0;

void build_loop_arr(int y, int x, int y0, int x0) {
    int y1 = y;
    int x1 = x;

    if(pipes_connected('N', y, x) && y - 1 != y0) {
        --y1;
    } else if(pipes_connected('S', y, x) && y + 1 != y0) {
        ++y1;
    } else if(pipes_connected('E', y, x) && x + 1 != x0) {
        ++x1;
    } else if(pipes_connected('W', y, x) && x - 1 != x0) {
        --x1;
    }

    char pipe = loop.pipes[y1][x1];
    loop_arr = realloc(loop_arr, sizeof(struct loop_piece) * ++loop_arr_length);
    loop_arr[loop_arr_length - 1].pipe = pipe;
    loop_arr[loop_arr_length - 1].y = y1;
    loop_arr[loop_arr_length - 1].x = x1;

    if(!(y1 == loop.start_y && x1 == loop.start_x))
        build_loop_arr(y1, x1, y, x);
}

int gold() {
    build_loop_arr(loop.start_y, loop.start_x, loop.start_y, loop.start_x);
    //duplicate first entry
    loop_arr = realloc(loop_arr, sizeof(struct loop_piece) * ++loop_arr_length);
    loop_arr[loop_arr_length - 1].pipe = loop_arr[0].pipe;
    loop_arr[loop_arr_length - 1].y = loop_arr[0].y;
    loop_arr[loop_arr_length - 1].x = loop_arr[0].x;

    // printf("loop length = %d\n", loop_arr_length);
    // for(int i = 0; i < loop_arr_length; ++i)
    //     printf("%c (%d,%d)\n", loop_arr[i].pipe, loop_arr[i].y, loop_arr[i].x);
    // printf("\n");

    int area = 0;

    for(int i = 0; i < loop_arr_length - 1; ++i) {
        // printf("%d %d %d %d\n", loop_arr[i].y, loop_arr[i + 1].x, loop_arr[i].x, loop_arr[i + 1].y);
        area += loop_arr[i].y * loop_arr[i + 1].x - loop_arr[i].x * loop_arr[i + 1].y;
    }

    area = abs(area);
    area -= (loop_arr_length - 1);
    area /= 2;
    ++area;

    return area;
}

int main()
{
    char buff[COLS];
    int height = 0;
    while (fgets(buff, COLS, stdin) != NULL) {
        int width = 0;
        for(int i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            loop.pipes[height][width] = buff[i];
            if(buff[i] == 'S') {
                loop.start_x = width;
                loop.start_y = height;
            }
            ++width;
        }
        loop.width = width;
        ++height;
    }
    loop.height = height;
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}