#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
 
#define ROWS 150
#define COLS 150

struct grid {
    int height;
    int width;
    char data[COLS][ROWS];
    int energy[COLS][ROWS];
};

struct grid *grid;

enum ENTER_FROM { UP, DOWN, LEFT, RIGHT };

bool move(enum ENTER_FROM dir, int *y, int *x) {
    int y1 = *y;
    int x1 = *x;
    switch (dir) {
        case UP: --y1; break;
        case DOWN: ++y1; break;
        case LEFT: --x1; break;
        case RIGHT: ++x1; break;
    }

    // printf("y:%d x:%d\n", y1, x1);

    if(y1 < 0 || y1 >= grid->height || x1 < 0 || x1 >= grid->width)
        return false;

    *y = y1;
    *x = x1;
    return true;
}

void trace_beam(enum ENTER_FROM dir, int y, int x) {
    if(move(dir, &y, &x)) {
        if(grid->energy[y][x] & 1 << dir)
            return;

        grid->energy[y][x] |= 1 << dir;

        if(grid->data[y][x] == '|') {
            if(dir == LEFT || dir == RIGHT) {
                trace_beam(UP, y, x);
                trace_beam(DOWN, y, x);
                return;
            }
        }
        
        if(grid->data[y][x] == '-') {
            if(dir == UP || dir == DOWN) {
                trace_beam(LEFT, y, x);
                trace_beam(RIGHT, y, x);
                return;
            }
        }
        
        if(grid->data[y][x] == '/') {
            if(dir == UP) { trace_beam(RIGHT, y, x); return; }
            if(dir == DOWN) { trace_beam(LEFT, y, x); return; }
            if(dir == LEFT) { trace_beam(DOWN, y, x); return; }
            if(dir == RIGHT) { trace_beam(UP, y, x); return; }
        }
        
        if(grid->data[y][x] == '\\') {
            if(dir == UP) { trace_beam(LEFT, y, x); return; }
            if(dir == DOWN) { trace_beam(RIGHT, y, x); return; }
            if(dir == LEFT) { trace_beam(UP, y, x); return; }
            if(dir == RIGHT) { trace_beam(DOWN, y, x); return; }
        }

        trace_beam(dir, y, x);
    }
}

int silver() {
    trace_beam(RIGHT, 0, -1);

    int sum = 0;
    for(int y = 0; y < grid->height; ++y)
        for(int x = 0; x < grid->width; ++x)
            if(grid->energy[y][x])
                sum += 1;

    return sum;
}

int energy_sum() {
    int sum = 0;
    for(int y = 0; y < grid->height; ++y)
        for(int x = 0; x < grid->width; ++x)
            if(grid->energy[y][x])
                sum += 1;
    return sum;
}

int gold() {
    int largest_sum = 0;
    for(int y = 0; y < grid->height; ++y) {
        //left
        memset(grid->energy, 0, sizeof(grid->energy));
        trace_beam(RIGHT, y, -1);
        int sum = energy_sum();
        if(sum > largest_sum)
            largest_sum = sum;

        //right
        memset(grid->energy, 0, sizeof(grid->energy));
        trace_beam(LEFT, y, grid->width);
        sum = energy_sum();
        if(sum > largest_sum)
            largest_sum = sum;
    }

    for(int x = 0; x < grid->width; ++x) {
        //top
        memset(grid->energy, 0, sizeof(grid->energy));
        trace_beam(DOWN, -1, x);
        int sum = energy_sum();
        if(sum > largest_sum)
            largest_sum = sum;

        //bottom
        memset(grid->energy, 0, sizeof(grid->energy));
        trace_beam(UP, grid->height, x);
        sum = energy_sum();
        if(sum > largest_sum)
            largest_sum = sum;
    }

    return largest_sum;
}

int main()
{
    grid = malloc(sizeof(struct grid));
    memset(grid, 0, sizeof(struct grid));

    char *buff = malloc(COLS);
    while (fgets(buff, COLS, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            grid->data[grid->height][i] = buff[i];
        }
        grid->width = i;
        grid->height++;
    }

    // for(int y = 0; y < grid->height; ++y) {
    //     for(int x = 0; x < grid->width; ++x)
    //         printf("%01X ", grid->energy[y][x]);
    //     printf("\n");
    // }

    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}