#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 1000
#define COLS 400

struct node;

struct node {
    char label[3];
    struct node *left;
    struct node *right;
};

char *pattern;
int pattern_length;

struct node *nodes = NULL;
int nodes_count = 0;

char next_direction(int *pos, int *steps) {
    if(*pos == pattern_length)
        *pos = 0;
    ++(*steps);
    // printf("retrieving %c\n", pattern[*pos]);
    return pattern[(*pos)++];
}

int silver() {
    struct node *cur_node;
    for(int i = 0; i < nodes_count; ++i) {
        if(!memcmp(nodes[i].label, "AAA", 3)) {
            cur_node = &nodes[i];
            break;
        }
    }

    int pos = 0;
    int steps = 0;
    while(memcmp(cur_node, "ZZZ", 3)) {
        for(int j = 0; j < 3; ++j)
            printf("%c", cur_node->label[j]);
        printf("\n");
        if(cur_node->left == NULL)
            printf("left null\n");
        if(cur_node->right == NULL)
            printf("right null\n");

        if(next_direction(&pos, &steps) == 'L')
            cur_node = cur_node->left;
        else
            cur_node = cur_node->right;
        // for(int j = 0; j < 3; ++j)
        //     printf("%c", cur_node->label[j]);
        // printf("\n");
    }

    return steps;
}

int main()
{
    char buff[COLS];
    fgets(buff, COLS, stdin); //pattern
    pattern_length = strlen(buff) - 1;
    pattern = malloc(sizeof(char) * pattern_length);
    memcpy(pattern, buff, pattern_length);

    fgets(buff, COLS, stdin); //empty line

    while (fgets(buff, COLS, stdin) != NULL) {
        char *src = &buff[0];
        char *left = &buff[7];
        char *right = &buff[12];

        struct node *cur_node;

        for(int i = 0; i < 3; ++i)
            printf("%c", src[i]);
        printf(" ");

        bool found_src = false;
        for(int i = 0; i < nodes_count; ++i) {
            if(!memcmp(src, nodes[i].label, 3)) {
                found_src = true;
                cur_node = &nodes[i];
                break;
            }
        }

        if(!found_src) {
            nodes = realloc(nodes, sizeof(struct node) * ++nodes_count);
            cur_node = &nodes[nodes_count - 1];
            memcpy(cur_node->label, src, 3);
            cur_node->left = NULL;
            cur_node->right = NULL;
        }

        bool found_left = false;
        for(int i = 0; i < nodes_count; ++i) {
            if(!memcmp(left, nodes[i].label, 3)) {
                found_left = true;
                cur_node->left = &nodes[i];
                break;
            }
        }

        if(!found_left) {
            nodes = realloc(nodes, sizeof(struct node) * ++nodes_count);
            memcpy(nodes[nodes_count - 1].label, left, 3);
            nodes[nodes_count - 1].left = NULL;
            nodes[nodes_count - 1].right = NULL;
            cur_node->left = &nodes[nodes_count - 1];
            printf("creating left ");
        }

        bool found_right = false;
        for(int i = 0; i < nodes_count; ++i) {
            if(!memcmp(right, nodes[i].label, 3)) {
                found_right = true;
                cur_node->right = &nodes[i];
                break;
            }
        }

        if(!found_right) {
            nodes = realloc(nodes, sizeof(struct node) * ++nodes_count);
            memcpy(nodes[nodes_count - 1].label, right, 3);
            nodes[nodes_count - 1].left = NULL;
            nodes[nodes_count - 1].right = NULL;
            cur_node->right = &nodes[nodes_count - 1];
            printf("creating right");
        }
        printf("\n");
    }

    // for(int i = 0; i < pattern_length; ++i)
    //         printf("%c", pattern[i]);
    // printf("\n");

    // for(int i = 0; i < nodes_count; ++i) {
    //     for(int j = 0; j < 3; ++j)
    //         printf("%c", nodes[i].label[j]);
    //     printf("\n");
    // }
    
    // printf("%d\n", silver());
    
    // printf("%d\n", gold());

    return 0;
}