#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
 
#define ROWS 1000
#define COLS 400

struct raw_node {
    uint32_t label;
    uint32_t left;
    uint32_t right;
};

struct node {
    uint32_t label;
    int left_idx;
    int right_idx;
};

struct graph {
    struct node *nodes;
    int nodes_count;
};

int find_node(struct graph *graph, uint32_t label) {
    for(int i = 0; i < graph->nodes_count; ++i)
        if(graph->nodes[i].label == label)
            return i;
    return -1;
}

int add_node(struct graph **graph, uint32_t label) {
    int idx;
    if((idx = find_node(*graph, label)) < 0) {
        idx = (*graph)->nodes_count;
        (*graph)->nodes = realloc((*graph)->nodes, sizeof(struct node) * ++(*graph)->nodes_count);
        struct node *node = &(*graph)->nodes[idx];
        node->label = label;
        node->left_idx = -1;
        node->right_idx = -1;
    }

    return idx;
}

void add_raw_node(struct graph **graph, struct raw_node *raw_node) {
    int idx = add_node(graph, raw_node->label);
    if((*graph)->nodes[idx].left_idx < 0){
        uint32_t lidx = add_node(graph, raw_node->left);
        (*graph)->nodes[idx].left_idx = lidx;
    } 
    if((*graph)->nodes[idx].right_idx < 0){
        uint32_t ridx = add_node(graph, raw_node->right);
        (*graph)->nodes[idx].right_idx = ridx;
    }
};

void print_graph(struct graph *graph) {
    for(int i = 0; i < graph->nodes_count; ++i) {
        char src[4] = {0};
        char left[4] = {0};
        char right[4] = {0};

        memcpy(src, &graph->nodes[i].label, 3);
        struct node *node_left = &graph->nodes[graph->nodes[i].left_idx];
        memcpy(left, &node_left->label, 3);
        struct node *node_right = &graph->nodes[graph->nodes[i].right_idx];
        memcpy(right, &node_right->label, 3);

        // printf("%s = (%s, %s)\n", src, left, right);
        // printf("%d = (%d, %d)\n", i, graph->nodes[i].left_idx, graph->nodes[i].right_idx);
    }
}

char *pattern;
int pattern_length;

char next_direction(int *pos, long long int *steps) {
    if(*pos == pattern_length)
        *pos = 0;
    ++(*steps);
    // printf("retrieving %c\n", pattern[*pos]);
    return pattern[(*pos)++];
}

long long int silver(struct graph *graph) {
    uint32_t start;
    memcpy(&start, "AAA", 3);
    uint32_t end;
    memcpy(&end, "ZZZ", 3);
    int idx = find_node(graph, start);

    int pos = 0;
    long long int steps = 0;
    while(graph->nodes[idx].label != end) {
        // printf("idx: %d steps: %d\n", idx, steps);
        if(next_direction(&pos, &steps) == 'L')
            idx = graph->nodes[idx].left_idx;
        else
            idx = graph->nodes[idx].right_idx;
    }

    return steps;
}

bool compare_third(uint32_t label, char c) {
    return ((label >> 16) & 0xFF) == c;
}

bool all_end(struct graph *graph, int *idx, int idx_count, long long int steps, int *start_idx, int *cycles) {
    bool all_end = true;
    for(int i = 0; i < idx_count; ++i) {
        bool end = compare_third(graph->nodes[idx[i]].label, 'Z');
        
        if(end && start_idx[i] != 0 && cycles[i] == 0)
            cycles[i] = steps - start_idx[i];
        
        if(end && start_idx[i] == 0)
            start_idx[i] = steps;
            
        all_end &= cycles[i];
    }
    return all_end;
}

long long int LCM(long long int a, long long int b) {
    long long int i, gcd, max;
    max = (a > b) ? a : b;
    for(i = 1; i <= a && i <= b; ++i){
        if(a % i == 0 && b % i == 0)
            gcd = i;
    }

    max = (a * b) / gcd;

    return max;
}

long long int gold(struct graph *graph) {
    uint32_t end;
    memcpy(&end, "ZZZ", 3);

    int *idx = NULL;
    int idx_count = 0;
    for(int i = 0; i < graph->nodes_count; ++i) {
        if(compare_third(graph->nodes[i].label, 'A')) {
            idx = realloc(idx, sizeof(int) * ++idx_count);
            idx[idx_count - 1] = i;
        }
    }

    int *start_idx = malloc(sizeof(int) * idx_count);
    memset(start_idx, 0, sizeof(int) * idx_count);

    int *cycles = malloc(sizeof(int) * idx_count);
    memset(cycles, 0, sizeof(int) * idx_count);

    //need to find the rate of change from Z to Z for all nodes then mod them together to determine # of steps to skip

    int pos = 0;
    long long int steps = 0;
    while(!all_end(graph, idx, idx_count, steps, start_idx, cycles)) {

        if(next_direction(&pos, &steps) == 'L'){
            for(int i = 0; i < idx_count; ++i)
                idx[i] = graph->nodes[idx[i]].left_idx;
            // idx = graph->nodes[idx].left_idx;
        }
        else {
            for(int i = 0; i < idx_count; ++i)
                idx[i] = graph->nodes[idx[i]].right_idx;
            // idx = graph->nodes[idx].right_idx;
        }
    }

    long long int max = cycles[0];
    for(int i = 1; i < idx_count; ++i)
        max = LCM(max, cycles[i]);

    return max;
}

int main()
{
    char buff[COLS];
    fgets(buff, COLS, stdin); //pattern
    pattern_length = strlen(buff) - 1;
    pattern = malloc(sizeof(char) * pattern_length);
    memcpy(pattern, buff, pattern_length);

    fgets(buff, COLS, stdin); //empty line

    struct graph *graph = malloc(sizeof(struct graph));
    memset(graph, 0, sizeof(struct graph));

    while (fgets(buff, COLS, stdin) != NULL) {
        char *src = &buff[0];
        char *left = &buff[7];
        char *right = &buff[12];

        struct raw_node raw_node = {0};
        memcpy(&raw_node.label, src, 3);
        memcpy(&raw_node.left, left, 3);
        memcpy(&raw_node.right, right, 3);

        add_raw_node(&graph, &raw_node);
    }

    // print_graph(&graph);
    
    // printf("%d\n", silver(&graph));

    printf("%lld\n", gold(graph));

    return 0;
}