#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 400
#define COLS 400

struct card {
    int winning_numbers;
    int winners_count;
    int *winners;
    int numbers_count;
    int *numbers;
};

struct card *cards = NULL;
int cards_count;

int intpow(int base, int exp) {
    int result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

int silver() {
    int sum = 0;
    for(int i = 0; i < cards_count; ++i) {
        for(int w = 0; w < cards[i].winners_count; ++w)
            for(int n = 0; n < cards[i].numbers_count; ++n)
                if(cards[i].winners[w] == cards[i].numbers[n])
                    ++cards[i].winning_numbers;
        if(cards[i].winning_numbers)
            sum += intpow(2, cards[i].winning_numbers - 1);
    }

    return sum;
}

int gold() {
    int *card_quantity = malloc(sizeof(int) * cards_count);
    for(int i = 0; i < cards_count; ++i)
        card_quantity[i] = 1;

    for(int i = 0; i < cards_count; ++i) {
        for(int j = i + 1; j < i + 1 + cards[i].winning_numbers; ++j) {
            card_quantity[j] += card_quantity[i];
        }
    }

    int sum = 0;
    for(int i = 0; i < cards_count; ++i)
        sum += card_quantity[i];

    return sum;
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        cards = realloc(cards, sizeof(struct card) * ++cards_count);
        cards[cards_count - 1].winning_numbers = 0;
        cards[cards_count - 1].winners_count = 0;
        cards[cards_count - 1].winners = NULL;
        cards[cards_count - 1].numbers_count = 0;
        cards[cards_count - 1].numbers = NULL;

        int i = 0;
        while(buff[i++] != ':') {} //skip past card label

        bool winners = true;
        for(; buff[i] != '\0' && buff[i] != '\n'; ++i) {
            while(buff[i] < '0' || buff[i] > '9') { //arrive at next number
                if(buff[i] == '|')
                    winners = false;
                ++i;
            }

            char *endptr;
            int val = strtoll(&buff[i], &endptr, 10);
            i = endptr - buff;

            //store
            struct card *c = &cards[cards_count - 1];
            if(winners) {
                c->winners = realloc(c->winners, sizeof(int) * ++c->winners_count);
                c->winners[c->winners_count - 1] = val;
            } else {
                c->numbers = realloc(c->numbers, sizeof(int) * ++c->numbers_count);
                c->numbers[c->numbers_count - 1] = val;
            }
        }
    }

    // for(int i = 0; i < cards_count; ++i) {
    //     printf("card %d:", i+1);
    //     for(int w = 0; w < cards[i].winners_count; ++w)
    //         printf(" %d", cards[i].winners[w]);
    //     printf(" |");
    //     for(int n = 0; n < cards[i].numbers_count; ++n)
    //         printf(" %d", cards[i].numbers[n]);
    //     printf("\n");
    // }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}