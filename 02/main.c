#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 200
#define COLS 400

struct round {
    int red;
    int green;
    int blue;
};

struct game {
    int round_count;
    struct round *rounds;
};

struct game games[ROWS];
int game_count = 0;

int silver() {
    int max_red = 12;
    int max_green = 13;
    int max_blue = 14;

    int sum = 0;
    for(int i = 0; i < game_count; ++i) {
        int largest_red = 0;
        int largest_green = 0;
        int largest_blue = 0;
        for(int j = 0; j < games[i].round_count; ++j) {
            if(games[i].rounds[j].red > largest_red)
                largest_red = games[i].rounds[j].red;
            if(games[i].rounds[j].green > largest_green)
                largest_green = games[i].rounds[j].green;
            if(games[i].rounds[j].blue > largest_blue)
                largest_blue = games[i].rounds[j].blue;
        }

        if(largest_red > max_red)
            continue;
        if(largest_green > max_green)
            continue;
        if(largest_blue > max_blue)
            continue;

        sum += i + 1;
    }

    return sum;
}

int gold() {
    int sum = 0;
    for(int i = 0; i < game_count; ++i) {
        int largest_red = 0;
        int largest_green = 0;
        int largest_blue = 0;
        for(int j = 0; j < games[i].round_count; ++j) {
            if(games[i].rounds[j].red > largest_red)
                largest_red = games[i].rounds[j].red;
            if(games[i].rounds[j].green > largest_green)
                largest_green = games[i].rounds[j].green;
            if(games[i].rounds[j].blue > largest_blue)
                largest_blue = games[i].rounds[j].blue;
        }

        sum += largest_red * largest_green * largest_blue;
    }

    return sum;
}

int nxt_number(char **str, bool *round_end) {
    while((**str < '0' || **str > '9') && **str != '\0') {
        if(**str == ';')
            *round_end = true;
        (*str)++;
    }

    if(**str == '\0')
        return 0;
    
    return 1;
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        games[game_count].round_count = 0;
        games[game_count].rounds = NULL;

        char *pos = buff;
        bool round_end = true;
        //consume game #
        nxt_number(&pos, &round_end);
        strtoll(pos, &pos, 10);

        while(nxt_number(&pos, &round_end)) {
            if(round_end) {
                ++games[game_count].round_count;
                games[game_count].rounds = realloc(games[game_count].rounds, sizeof(struct round) * games[game_count].round_count);
                games[game_count].rounds[games[game_count].round_count - 1].red = 0;
                games[game_count].rounds[games[game_count].round_count - 1].green = 0;
                games[game_count].rounds[games[game_count].round_count - 1].blue = 0;
                round_end = false;
            }

            int val = strtoll(pos, &pos, 10);
            if(*(pos + 1) == 'r')
                games[game_count].rounds[games[game_count].round_count - 1].red = val;
            if(*(pos + 1) == 'g')
                games[game_count].rounds[games[game_count].round_count - 1].green = val;
            if(*(pos + 1) == 'b')
                games[game_count].rounds[games[game_count].round_count - 1].blue = val;
        }

        ++game_count;
    }

    // for(int i = 0; i < game_count; ++i) {
    //     printf("game %d\n", i + 1);
    //     for(int j = 0; j < games[i].round_count; ++j) {
    //         printf("  r: %d, g: %d, b: %d\n", games[i].rounds[j].red, games[i].rounds[j].green, games[i].rounds[j].blue);
    //     }
    // }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}