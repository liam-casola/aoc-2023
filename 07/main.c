#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define ROWS 1500
#define COLS 20

char labels[] = "AKQJT98765432";

struct hand {
    char cards[5];
    int bid;
    int rank;
};

struct hand hands[ROWS];
int hands_count = 0;

//types: 0: High card, 1: One pair 2: Two pair, 3: Three of a kind, 4: Full house, 5: Four of a kind, 6: Five of a kind
int hand_type(struct hand *h) {
    int count[13] = {0};
    //find quantity of each card
    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < sizeof(labels) - 1; ++j)
            if(h->cards[i] == labels[j])
                ++count[j];
    }

    //6: Five of a kind
    for(int i = 0; i < sizeof(count) / sizeof(int); ++i) {
        if(count[i] == 5)
            return 6;
    }

    //5: Four of a kind
    for(int i = 0; i < sizeof(count) / sizeof(int); ++i) {
        if(count[i] == 4)
            return 5;
    }

    {
    bool found_3 = false;
    bool found_2 = false;
    for(int i = 0; i < sizeof(count) / sizeof(int); ++i) {
        if(count[i] == 3)
            found_3 = true;
        if(count[i] == 2)
            found_2 = true;
    }
    //4: Full house
    if(found_3 && found_2)
        return 4;
    //3: Three of a kind
    if(found_3)
        return 3;
    }

    {
    int pair_count = 0;
    for(int i = 0; i < sizeof(count) / sizeof(int); ++i) {
        if(count[i] == 2)
            ++pair_count;
    }
    //2: Two pair
    if(pair_count == 2)
        return 2;
    //1: One pair
    if(pair_count == 1)
        return 1;
    }

    return 0;
}

int compare_hands(const void *a, const void *b) {
    struct hand ha = *((struct hand *)a);
    struct hand hb = *((struct hand *)b);

    if(hand_type(&ha) > hand_type(&hb))
        return 1;
    if(hand_type(&ha) < hand_type(&hb))
        return -1;
    //tie breaker
    for(int i = 0; i < 5; ++i) {
        int hav;
        int hbv;
        for(int j = 0; j < sizeof(labels) - 1; ++j) {
            if(ha.cards[i] == labels[j])
                hav = j;
            if(hb.cards[i] == labels[j])
                hbv = j;
        }
        if(hav < hbv)
            return 1;
        if(hav > hbv)
            return -1;
    }

    return 0;
}

int silver() {
    qsort(hands, hands_count, sizeof(struct hand), compare_hands);

    // for(int i = 0; i < hands_count; ++i){
    //     for(int j = 0; j < 5; ++j)
    //         printf("%c", hands[i].cards[j]);
    //     printf(" %d\n", hands[i].bid);
    // }

    int sum = 0;
    for(int i = 0; i < hands_count; ++i)
        sum += hands[i].bid * (i + 1);

    return sum;
}

#define JOKER_POS 12
char labels2[] = "AKQT98765432J";

//types: 0: High card, 1: One pair 2: Two pair, 3: Three of a kind, 4: Full house, 5: Four of a kind, 6: Five of a kind
int hand_type2(struct hand *h) {
    int count[13] = {0};
    //find quantity of each card
    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < sizeof(labels2) - 1; ++j)
            if(h->cards[i] == labels2[j])
                ++count[j];
    }

    //6: Five of a kind
    for(int i = 0; i < sizeof(count) / sizeof(int) - 1; ++i) {
        if(count[i] + count[JOKER_POS] == 5)
            return 6;
    }

    //5: Four of a kind
    for(int i = 0; i < sizeof(count) / sizeof(int) - 1; ++i) {
        if(count[i] + count[JOKER_POS] == 4)
            return 5;
    }

    //4: Full house (joker)
    {
    int pair_count = 0;
    for(int i = 0; i < sizeof(count) / sizeof(int) - 1; ++i) {
        if(count[i] == 2)
            ++pair_count;
    }
    //2: Two pair
    if(pair_count == 2 && count[JOKER_POS] == 1)
        return 4;
    }

    {
    bool found_3 = false;
    bool found_2 = false;
    for(int i = 0; i < sizeof(count) / sizeof(int); ++i) {
        if(count[i] == 3)
            found_3 = true;
        if(count[i] == 2)
            found_2 = true;
    }
    //4: Full house
    if(found_3 && found_2)
        return 4;
    }

    //3: Three of a kind
    for(int i = 0; i < sizeof(count) / sizeof(int) - 1; ++i) {
        if(count[i] + count[JOKER_POS] == 3)
            return 3;
    }

    {
    int pair_count = 0;
    bool joker = count[JOKER_POS]; //max 1
    for(int i = 0; i < sizeof(count) / sizeof(int) - 1; ++i) {
        if(count[i] == 2)
            ++pair_count;
        if(count[i] == 1 && joker) {
            joker = 0;
            ++pair_count;
        }
    }
    //2: Two pair
    if(pair_count == 2)
        return 2;
    //1: One pair
    if(pair_count == 1)
        return 1;
    }

    return 0;
}

int compare_hands2(const void *a, const void *b) {
    struct hand *ha = (struct hand *)a;
    struct hand *hb = (struct hand *)b;
    ha->rank = hand_type2(ha);
    hb->rank = hand_type2(hb);

    if(hand_type2(ha) > hand_type2(hb))
        return 1;
    if(hand_type2(ha) < hand_type2(hb))
        return -1;
    //tie breaker
    for(int i = 0; i < 5; ++i) {
        int hav;
        int hbv;
        for(int j = 0; j < sizeof(labels2) - 1; ++j) {
            if(ha->cards[i] == labels2[j])
                hav = j;
            if(hb->cards[i] == labels2[j])
                hbv = j;
        }
        if(hav < hbv)
            return 1;
        if(hav > hbv)
            return -1;
    }

    return 0;
}

int gold() {
    qsort(hands, hands_count, sizeof(struct hand), compare_hands2);

    // for(int i = 0; i < hands_count; ++i){
    //     for(int j = 0; j < 5; ++j)
    //         printf("%c", hands[i].cards[j]);
    //     printf(" %d %d\n", hands[i].rank, hands[i].bid);
    // }

    int sum = 0;
    for(int i = 0; i < hands_count; ++i)
        sum += hands[i].bid * (i + 1);

    return sum;
}

int main()
{
    char buff[COLS];
    while (fgets(buff, COLS, stdin) != NULL) {
        memcpy(hands[hands_count].cards, buff, 5);
        hands[hands_count].bid = atoi(&buff[5]);
        ++hands_count;
    }

    // for(int i = 0; i < hands_count; ++i){
    //     for(int j = 0; j < 5; ++j)
    //         printf("%c", hands[i].cards[j]);
    //     printf(" %d\n", hands[i].bid);
    // }
    
    printf("%d\n", silver());
    
    printf("%d\n", gold());

    return 0;
}